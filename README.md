How to install the required files for lid ->

1. Have to create a virtual (conda) env, then get py 3.11 cuz with other py versions number, numpy, tts doesn’t work perfectly.. you have to get to install tts or tts - 0.12 version with py 3.11.. whatever works for your system and clone the package from gitlab. Change into the current directory.

** Under development **
** have used another library that works perfect for smaller/longer texts as it is using optional n-grams where n = {1, 2, 3, 4, 5} **
2. Then for language detection, you need pycld2 as polyglot works on pycld2 so a pip install won’t do the task, you need to do a wget download and install the patch 44.

mkdir $MYVIRTUALENV/src
cd $MYVIRTUALENV/src
wget https://files.pythonhosted.org/packages/21/d2/8b0def84a53c88d0eb27c67b05269fbd16ad68df8c78849e7b5d65e6aec3/pycld2-0.41.tar.gz
tar -xzvf pycld2-0.41.tar.gz
cd pycld2-0.41
wget https://patch-diff.githubusercontent.com/raw/aboSamoor/pycld2/pull/44.patch
patch -p1 < 44.patch
CFLAGS="-w -O2 -fPIC -march=armv8-a" pip install --no-cache-dir --no-deps --no-build-isolation  .
** -- **

3. run the command
``./dataset.command`` 
in your terminal
