import streamlit as st
from audiorecorder import audiorecorder
import numpy as np
import openai
from TTS.api import TTS
import glob
import sys
import os
import regex
import pycld2 as cld2
from pydub import AudioSegment
from dotenv import load_dotenv
import time
from lingua import Language, LanguageDetectorBuilder
languages = [Language.ENGLISH, Language.FRENCH, Language.GERMAN, Language.SPANISH]
load_dotenv()

# init TTS
tts = TTS()
# Choosing which models to select in tts
models_list = ['tts_models/en/ljspeech/glow-tts', 'tts_models/fr/mai/tacotron2-DDC', 'tts_models/es/mai/tacotron2-DDC', 'tts_models/de/thorsten/tacotron2-DCA']
en_tts = TTS(model_name=models_list[0], progress_bar=True, gpu=False)
fr_tts = TTS(model_name=models_list[1], progress_bar=True, gpu=False)
es_tts = TTS(model_name=models_list[2], progress_bar=True, gpu=False)
de_tts = TTS(model_name=models_list[3], progress_bar=True, gpu=False)
models_dict = {'tts_models/en/ljspeech/glow-tts': en_tts, 'tts_models/fr/mai/tacotron2-DDC': fr_tts, 'tts_models/es/mai/tacotron2-DDC': es_tts, 'tts_models/de/thorsten/tacotron2-DCA': de_tts}

with st.container():
    agree = st.checkbox('Do you want to hear back what you said?')
    if agree:
        
#         data = u"""A accès aux chiens et aux frontaux qui lui ont été il peut consulter et modifier ses collections
# et exporter Cet article concerne le pays européen aujourd'hui appelé République française. Pour d'autres usages du nom France, Pour une aide rapide et effective, veuiller trouver votre aide
# dans le menu ci-dessus. Motoring events began soon after the construction of the first successful gasoline-fueled automobiles.
# The quick brown fox jumped over the lazy dog. Estoy leyendo un libro sobre Paraguay y estoy aprendiendo cosas interesantes del país. Ahora sé
# que en Paraguay hay dos idiomas oficiales, el español y el guaraní. El guaraní es una lengua complicada; su pronunciación es difícil. Es muy antiguo y diferente, parece interesante. 
# Feste und Feiertage in Deutschland. Betäubungsmittelverschreibungsverordnung. Bezirksschornsteinfegermeister. Donaudampfschifffahrtsgesellschaftskapitän.
# Rindfleischetikettierungsüberwachungsaufgabenübertragungsgesetz."""
        data = st.text_input("Multi-language input")
        if data:
            lan_arr = []
            text_arr = []
            detector = LanguageDetectorBuilder.from_languages(*languages).build()
            # detector = LanguageDetectorBuilder.from_languages(*languages).with_minimum_relative_distance(0.1).build()
            # sentence = "Parlez-vous français? Ich spreche Französisch nur ein bisschen. A little bit is better than nothing."
            for result in detector.detect_multiple_languages_of(data):
                if result.language.name == 'ENGLISH':
                    lan_arr.append('en')
                if result.language.name == 'FRENCH':
                    lan_arr.append('fr')
                if result.language.name == 'GERMAN':
                    lan_arr.append('de')
                if result.language.name == 'SPANISH':
                    lan_arr.append('es')
                text_arr.append(data[result.start_index:result.end_index])

            print("The languages are", lan_arr)
            print("The text are", text_arr)

            models = []

            for code in lan_arr:
                for model_name in models_list:
                    country_code = model_name.split("/")[1]
                    if country_code == code:
                        # print("{0}: {1}".format(code, model_name))
                        models.append(models_dict[model_name])
                        break

            print()
            print("Unique Languages ({0}):".format(len(models)))
            print("The models are", models)

            counter = 0

            model_instances = zip(models,text_arr)
            print(model_instances)
            print(text_arr)

            # Init TTS with the target model name
            for model, text in model_instances:
                tts = model
                # Run TTS
                counter = counter + 1
                file_path = "intro" + str(counter) + ".wav"
                tts.tts_to_file(text=text, file_path=file_path)
                print(time.time())
                print(counter)
                
            # combine audio files
            filenames = glob.glob("intro*.wav")
            filenames = sorted(filenames)
            print(filenames)
            combined_sounds = AudioSegment.empty()

            for filename in filenames:
                combined_sounds = combined_sounds + AudioSegment.from_wav(filename)


            combined_sounds.export("lid.wav", format="wav")

            for fname in filenames:
                os.remove(fname)




