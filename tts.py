from TTS.api import TTS
from dotenv import load_dotenv
# Running a multi-speaker and multi-lingual model
load_dotenv()
# # Init TTS
# tts_obj = TTS()
# # List available 🐸TTS models and choose the first one
# model_name = tts_obj.list_models()[0]
# # print(model_name[0])


# # Run TTS
# tts = TTS(model_name=model_name, progress_bar=False, gpu=False)
# # ❗ Since this model is multi-speaker and multi-lingual, we must set the target speaker and the language
# # Text to speech with a numpy output
# wav = tts.tts("This is a test! This is also a test!!", speaker=tts.speakers[0], language=tts.languages[0])
# # Text to speech to a file
# tts.tts_to_file(text="Hello world! This is a test! This is also a test!!", speaker=tts.speakers[0], language=tts.languages[0], file_path="output.wav")
# tts.tts_to_file("This is voice cloning.", speaker_wav="my/cloning/audio.wav", language="en", file_path="output1.wav")
# tts.tts_to_file("C'est le clonage de la voix.", speaker=tts.speakers[2], language="fr-fr", file_path="output2.wav")
# tts.tts_to_file("Isso é clonagem de voz.", speaker=tts.speakers[1], language="pt-br", file_path="output3.wav")

# # Example voice conversion converting speaker of the `source_wav` to the speaker of the `target_wav`

# tts = TTS(model_name="voice_conversion_models/multilingual/vctk/freevc24", progress_bar=True, gpu=False)
# tts.voice_conversion_to_file(source_wav="output3.wav", target_wav="output2.wav", file_path="output3.wav")

# # Example voice cloning by a single speaker TTS model combining with the voice conversion model. This way, you can
# # clone voices by using any model in 🐸TTS.

# tts = TTS("tts_models/de/thorsten/tacotron2-DDC")
# tts.tts_with_vc_to_file(
#     "Wie sage ich auf Italienisch, dass ich dich liebe?",
#     speaker_wav="output3.wav",
#     file_path="output.wav"
# )

# # Example text to speech using [🐸Coqui Studio](https://coqui.ai) models.

# # You can use all of your available speakers in the studio.
# # [🐸Coqui Studio](https://coqui.ai) API token is required. You can get it from the [account page](https://coqui.ai/account).
# # You should set the `COQUI_STUDIO_TOKEN` environment variable to use the API token.

# # If you have a valid API token set you will see the studio speakers as separate models in the list.
# # The name format is coqui_studio/en/<studio_speaker_name>/coqui_studio
# obj_tts = TTS()
# models = obj_tts.list_models()
# print(models)
# # Init TTS with the target studio speaker
# tts = TTS(model_name='tts_models/fa/custom/glow-tts', progress_bar=False, gpu=False)

# # Run TTS with emotion and speed control
# tts.tts_to_file(text="This is a test.", file_path="output4.wav", emotion="Happy", speed=1.5)


# #Example text to speech using **Fairseq models in ~1100 languages** 🤯.

# #For these models use the following name format: `tts_models/<lang-iso_code>/fairseq/vits`.
# #You can find the list of language ISO codes [here](https://dl.fbaipublicfiles.com/mms/tts/all-tts-languages.html) and learn about the Fairseq models [here](https://github.com/facebookresearch/fairseq/tree/main/examples/mms).

# # TTS with on the fly voice conversion
# api = TTS("tts_models/deu/fairseq/vits")
# api.tts_with_vc_to_file(
#     "Wie sage ich auf Italienisch, dass ich dich liebe?",
#     speaker_wav="output3.wav",
#     file_path="output5.wav"
# )

from TTS.api import CS_API

# Init 🐸 Coqui Studio API
# you can either set the API token as an environment variable `COQUI_STUDIO_TOKEN` or pass it as an argument.
token="PL1PURhQCBOYTbEzhudkca09aJ5eWQnNRVvgKfGONWzRIhnUX9u5ZzaiuyqnTZMa"
# # XTTS - Best quality and life-like speech in EN
# api = CS_API(api_token=token, model="XTTS")
# api.speakers  # all the speakers are available with all the models.
# api.list_speakers()
# api.list_voices()
# wav, sample_rate = api.tts(text="This is a test.", speaker=api.speakers[0].name, emotion="Happy", speed=1.5)

# # XTTS-multilingual - Multilingual XTTS with [en, de, es, fr, it, pt, ...] (more langs coming soon)
# api = CS_API(api_token=token, model="XTTS-multilingual")
# print(api.speakers)
# print(api.list_speakers())
# print(api.list_voices())
# wav, sample_rate = api.tts(text="This is a test.", speaker=api.speakers[0].name, emotion="Happy", speed=1.5)

# # V1 - Fast and lightweight TTS in EN with emotion control.
# api = CS_API(api_token=token, model="V1")
# api.speakers
# api.emotions  # emotions are only for the V1 model.
# api.list_speakers()
# api.list_voices()
# wav, sample_rate = api.tts(text="This is a test.", speaker=api.speakers[0].name, emotion="Happy", speed=1.5)