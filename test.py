import streamlit as st
from audiorecorder import audiorecorder
import numpy as np
import openai
from TTS.api import TTS
import glob
import sys
import os
import regex
import pycld2 as cld2
from pydub import AudioSegment
from dotenv import load_dotenv
import time
load_dotenv()

openai.api_key = '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'
# Transcribe audio
def transcribe_audio(filename):
    with open(filename, "rb") as audio_file:
        transcript = openai.Audio.transcribe("whisper-1", audio_file)
        return transcript["text"]

# init TTS
tts = TTS()
# Choosing which models to select in tts
models_list = ['tts_models/en/ek1/tacotron2', 'tts_models/fr/mai/tacotron2-DDC', 'tts_models/es/mai/tacotron2-DDC', 'tts_models/de/thorsten/tacotron2-DCA']
en_tts = TTS(model_name=models_list[0], progress_bar=True, gpu=False)
fr_tts = TTS(model_name=models_list[1], progress_bar=True, gpu=False)
es_tts = TTS(model_name=models_list[2], progress_bar=True, gpu=False)
de_tts = TTS(model_name=models_list[3], progress_bar=True, gpu=False)
models_dict = {'tts_models/en/ek1/tacotron2': en_tts, 'tts_models/fr/mai/tacotron2-DDC': fr_tts, 'tts_models/es/mai/tacotron2-DDC': es_tts, 'tts_models/de/thorsten/tacotron2-DCA': de_tts}

col1, col2, col3 = st.columns(3)
with st.container():
    with col1:
        st.subheader("Audio Input")
        audio = st.file_uploader("Upload an audio file (format wav)", type="wav")
        if "start_time" not in st.session_state:
            st.session_state.start_time = 0
        # getting the audio file
        if audio is not None:
            audio_file = audio.read()
            st.audio(audio_file, format="audio/wav", start_time=st.session_state.start_time)
            audio_bytes = audio.read()
            with open('new_audio.wav', mode='bx') as f:
                f.write(audio_bytes)
        
        




    with col2:
        audio = audiorecorder("Click to record", "Recording...")

        if len(audio) > 0:
            # To play audio in frontend:
            st.audio(audio.tobytes())
            
            # To save audio to a file:
            wav_file = open("new_audio.wav", "wb")
            wav_file.write(audio.tobytes())
            
                





    with col3:
        option = st.selectbox(
                'Select from the pre-recorded audio files',
                (None, 'myaudio.wav', 'youraudio.wav', 'theiraudio.wav', 'ouraudio.wav', 'whoseaudio.wav', 'hisaudio.wav', 'heraudio.wav'))
        if option:
            audio_file = open(option, 'rb')
            audio_bytes = audio_file.read()

            st.audio(audio_bytes, format='audio/wav')

            sample_rate = 44100  # 44100 samples per second
            seconds = 2  # Note duration of 2 seconds
            frequency_la = 440  # Our played note will be 440 Hz
            # Generate array with seconds*sample_rate steps, ranging between 0 and seconds
            t = np.linspace(0, seconds, seconds * sample_rate, False)
            # Generate a 440 Hz sine wave
            note_la = np.sin(frequency_la * t * 2 * np.pi)

            st.audio(note_la, sample_rate=sample_rate)
            with open('new_audio.wav', mode='bx') as f:
                f.write(audio_bytes)

# transcribe and show text (asr)
st.subheader("Transription of audio input")
transcription = ""
with st.container():
    if os.path.exists('new_audio.wav'):
        transcription = transcribe_audio('new_audio.wav')
        with open('new_audio.txt', mode='w') as f:
                    f.write(transcription)
        agree_asr = st.checkbox('transcribe audio?')
        if agree_asr:
            st.write(transcription)
        
        


# tts
#col4, col5, col6 = st.columns(3)
with st.container():

    
    agree = st.checkbox('Do you want to hear back what you said?')
    if agree:
        data = transcription
        data = u"""A accès aux chiens et aux frontaux qui lui ont été il peut consulter et modifier ses collections
et exporter Cet article concerne le pays européen aujourd'hui appelé République française. Pour d'autres usages du nom France, Pour une aide rapide et effective, veuiller trouver votre aide
dans le menu ci-dessus. Motoring events began soon after the construction of the first successful gasoline-fueled automobiles.
The quick brown fox jumped over the lazy dog. Estoy leyendo un libro sobre Paraguay y estoy aprendiendo cosas interesantes del país. Ahora sé
que en Paraguay hay dos idiomas oficiales, el español y el guaraní. El guaraní es una lengua complicada; su pronunciación es difícil. Es muy antiguo y diferente, parece interesante. 
Feste und Feiertage in Deutschland. Betäubungsmittelverschreibungsverordnung. Bezirksschornsteinfegermeister. Donaudampfschifffahrtsgesellschaftskapitän.
Rindfleischetikettierungsüberwachungsaufgabenübertragungsgesetz."""
        isReliable, textBytesFound, details, vectors = cld2.detect(data, returnVectors=True)
        print("The vectors are", vectors)
        # print(cld2.LANGUAGES) # french - fr, english - en, spanish - es, german - de
        lan_arr = [] # prints the model languages to be used
        text_arr = [] # prints the text that is detected to be of one language
        for index in range(len(vectors)):
            lan_arr.append(vectors[index][3])
            text_arr.append(data[vectors[index][0] : int(vectors[index][0]) + int(vectors[index][1])])

        print("The languages are", lan_arr)
        print("The text are", text_arr)



        models = []
        # print("Coqui models are", tts.list_models())


        for code in lan_arr:
            for model_name in models_list:
                country_code = model_name.split("/")[1]
                if country_code == code:
                    # print("{0}: {1}".format(code, model_name))
                    models.append(models_dict[model_name])
                    break

        print()
        print("Unique Languages ({0}):".format(len(models)))
        print("The models are", models)

        counter = 0

        model_instances = zip(models,text_arr)
        print(model_instances)
        print(text_arr)

        # Init TTS with the target model name
        for model, text in model_instances:
            tts = model
            # Run TTS
            counter = counter + 1
            file_path = "intro" + str(counter) + ".wav"
            tts.tts_to_file(text=text, file_path=file_path)
            print(time.time())
            print(counter)
            
        # combine audio files
        filenames = glob.glob("intro*.wav")
        filenames = sorted(filenames)
        print(filenames)
        combined_sounds = AudioSegment.empty()

        for filename in filenames:
            combined_sounds = combined_sounds + AudioSegment.from_wav(filename)


        combined_sounds.export("lid.wav", format="wav")

        for fname in filenames:
            os.remove(fname)




# # agree_cloning = st.checkbox('Do you want to clone your voice to somebody else?')
